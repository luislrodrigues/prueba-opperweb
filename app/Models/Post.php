<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Categoria;
use App\Models\Comentario;


class Post extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'posts';
    protected $fillable = [
        'categoria_id',
        'titulo',
        'contenido',
    ];

    public function categoria(){
        return $this->belongsTo(Categoria::class);
    }
    public function comentarios(){
        return $this->hasMany(Comentario::class);
    }
}
