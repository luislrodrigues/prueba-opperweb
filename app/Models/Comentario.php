<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Post;

class Comentario extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'comentarios';
    protected $fillable = [
        'post_id',
        'contenido'
    ];

    public function post(){
        return $this->belongsTo(Post::class);
    }
}
