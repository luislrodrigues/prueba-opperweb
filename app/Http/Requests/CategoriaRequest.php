<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\ApiResponser;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;
class CategoriaRequest extends FormRequest
{
    use ApiResponser;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                'nombre'         => ['bail','required','string',Rule::unique('categorias')->whereNull('deleted_at')],
                ];
              break;
            case 'PUT':
                return [
                    'nombre'     => ['bail','required','string',Rule::unique('categorias')->ignore(request('id'))->whereNull('deleted_at')],
                    ];
              break;
            default:
            return [
                'nombre'         => ['bail','required','string',Rule::unique('categorias')->whereNull('deleted_at')],
                ];
              break;
        }
    }
    public function failedValidation(ValidationValidator $validator) {
        $message = $validator->errors()->first();
        throw new HttpResponseException($this->showMessage($message, 500, false));
    }
}
