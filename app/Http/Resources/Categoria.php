<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Categoria extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                                     => $this->id,
            'nombre'                                 => $this->nombre,
            'fecha_creacion'                         => date_format($this->created_at, 'Y-m-d, h:m'),
            'fecha_actualizacion'                    => date_format($this->updated_at, 'Y-m-d, h:m'),
        ];
    }
}
