<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use App\Models\Comentario;
use App\Http\Resources\Comentario as ComentarioResource;
use App\Http\Resources\ComentarioCollection;
use App\Http\Requests\ComentarioRequest;


class ComentarioController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return $this->successResponse(new ComentarioCollection(Comentario::all()));
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(), 404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ComentarioRequest $request)
    {
        $data = $request->all();
        try{
            Comentario::create($data);
            return $this->showMessage('comentario creado exitosamente');
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(),409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $comentario = Comentario::findOrFail($id);
            return $this->successResponse(new ComentarioResource($comentario),'comentartio encontrado');
        } catch (\Exception $e) {
            return $this->errorResponse('comentario no encontrado',404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ComentarioRequest $request, $id)
    {
        $data = $request->all();
        try{
            $comentario = Comentario::findOrFail($id);
            $comentario->update($data);
            return $this->showMessage('comentario actualizado correctamente');
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(),404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $comentario=  Comentario::findOrFail($id);
            $comentario->delete();
            return $this->showMessage('comentario eliminado correctamente');
        } catch (\Exception $e) {
            return $this->errorResponse('comentario no encontrado',404);
        }
    }
}
