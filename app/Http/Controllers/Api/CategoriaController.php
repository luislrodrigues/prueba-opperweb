<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CategoriaRequest;
use App\Http\Resources\Categoria as CategoriaResource;
use App\Http\Resources\CategoriaCollection;
use App\Models\Categoria;
use App\Traits\ApiResponser;

class CategoriaController extends Controller
{
    use ApiResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return $this->successResponse(new CategoriaCollection(Categoria::all()));
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage(), 404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriaRequest $request)
    {
        $data = $request->all();
        try{
            Categoria::create($data);
            return $this->showMessage('categoria creada exitosamente');
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(),409);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $categoria = Categoria::findOrFail($id);
            return $this->successResponse(new CategoriaResource($categoria),'categoria encontrada');
        } catch (\Exception $e) {
            return $this->errorResponse('no se encontro la categoria',404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriaRequest $request, $id)
    {
        $data = $request->all();
        try{
            $categoria = Categoria::findOrFail($id);
            $categoria->update($data);
            return $this->showMessage('categoria actualizada correctamente');
        }catch(\Exception $e){
            return $this->errorResponse($e->getMessage(),404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $categoria = Categoria::findOrFail($id);
            $categoria->posts()->delete();
            $categoria->delete();
            return $this->showMessage('categoria eliminada correctamente');
        } catch (\Exception $e) {
            return $this->errorResponse('categoria no encontrada',404);
        }
    }
}
