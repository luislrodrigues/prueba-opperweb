<?php

namespace Database\Factories;

use App\Models\Comentario;
use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class ComentarioFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Comentario::class;

    public function definition()
    {
        return [
            'post_id'  => Post::all()->random()->id,
            'contenido'=> $this->faker->paragraph(),
        ];
    }
}
